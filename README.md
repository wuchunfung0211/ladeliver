Version:
pod: cocoapods-core-1.6.0.beta.2
xcode: Version 10.1 (10B61)

pod library:
'AlamofireObjectMapper', '~> 5.2'
'AlamofireImage', '~> 3.5'
'SnapKit', '~> 4.0.0'
'PromiseKit', '~> 6.0'
'R.swift'
'MBProgressHUD', '~> 1.1.0'