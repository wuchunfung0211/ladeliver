//
//  BaseService.swift
//  ladeliver
//
//  Created by Wu Chun Fung on 28/12/2018.
//  Copyright © 2018年 Wu Chun Fung. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class BaseService {
    
    var endpoint : String {
        return Base.EndPoint
    }
    
    internal func get(url: String) -> Promise<Any> {
        return request(.get, url: url)
    }
    
    fileprivate func request(_ method: Alamofire.HTTPMethod, url: String) -> Promise<Any> {
        UIApplication.showLoading()
        return Promise { seal in
            Alamofire.request(url, method: method).responseJSON { response in
                UIApplication.hideLoading()
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    seal.reject(error)
                }
            }
        }
    }
    
}
