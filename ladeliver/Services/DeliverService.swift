//
//  DeliverService.swift
//  ladeliver
//
//  Created by Wu Chun Fung on 28/12/2018.
//  Copyright © 2018年 Wu Chun Fung. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

fileprivate enum Path: String {
    case delivery = "/deliveries"
}

class DeliverService: BaseService {
    
    func getdeliveries() -> Promise<DeliveryResponse> {
        return Promise { seal in
            get(url: "\(endpoint)\(Path.delivery.rawValue)").done { result in
                guard let response = Mapper<DeliveryResponse>().map(JSONObject: ["data": result]) else {
                    seal.reject(NSError(domain: Path.delivery.rawValue, code: 000, userInfo: nil))
                    return
                }
                
                UIApplication.clearCachedData()
                UIApplication.cacheData(JSONObject: ["data": result])

                seal.fulfill(response)
            }.catch { error in
                seal.reject(error)
            }
        }
    }
    
}
