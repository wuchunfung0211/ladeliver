//
//  AppHelper.swift
//  ladeliver
//
//  Created by Wu Chun Fung on 28/12/2018.
//  Copyright © 2018年 Wu Chun Fung. All rights reserved.
//

import UIKit
import Foundation
import MBProgressHUD
import ObjectMapper

extension UIApplication {
    
    static var appDelegate: AppDelegate {
        return shared.delegate as! AppDelegate
    }
    
    static func showLoading() {
        if let window = UIApplication.appDelegate.window {
            MBProgressHUD.showAdded(to: window, animated: true)
        }
    }
    
    static func hideLoading() {
        if let window = UIApplication.appDelegate.window {
            MBProgressHUD.hide(for: window, animated: true)
        }
    }
    
    static func cacheData(JSONObject: Any?) {
        if let JSON = JSONObject as? [String: Any] {
            UserDefaults.standard.set(JSON, forKey: "delivery")
        }
    }
    
    static func clearCachedData() {
        UserDefaults.standard.removeObject(forKey: "delivery")
    }
    
    static func getCachedData() -> DeliveryResponse? {
        if let JSON = UserDefaults.standard.value(forKey: "delivery"),
            let data = Mapper<DeliveryResponse>().map(JSONObject: JSON) {
            return data
        }
        
        return nil
    }
    
}
