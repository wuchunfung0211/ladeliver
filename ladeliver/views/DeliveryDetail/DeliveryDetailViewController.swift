//
//  DeliveryDetailViewController.swift
//  ladeliver
//
//  Created by Wu Chun Fung on 28/12/2018.
//  Copyright © 2018年 Wu Chun Fung. All rights reserved.
//

import UIKit
import MapKit
import SnapKit

class DeliveryDetailViewController: BaseViewController {
    
    let regionRadius: CLLocationDistance = 1000
    
    var deliveryModel: DeliveryItem?
    var detailModel: LocationItem?
    var mapView: MKMapView!
    var addressLabel: UILabel!
    
    override func initUI() {
        super.initUI()
        
        title = R.string.localizable.deliveryDetail()
        
        mapView = MKMapView()
        
        view.addSubview(mapView)
        mapView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(view)
            make.left.equalTo(view)
            make.right.equalTo(view)
            make.height.equalTo(view).multipliedBy(0.45)
        }
        
        addressLabel = UILabel()
        addressLabel.numberOfLines = 0
        
        view.addSubview(addressLabel)
        addressLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(mapView.snp.bottom).offset(20)
            make.left.equalTo(view)
            make.right.equalTo(view)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView?.delegate = self
        
        addressLabel.text = "Address:\n\(detailModel?.address ?? "")"
        
        let initialLocation = CLLocation(latitude: detailModel?.lat ?? 0.0, longitude: detailModel?.lng ?? 0.0)
        centerMapOnLocation(location: initialLocation)
        setArtwork(location: initialLocation)
    }

}

extension DeliveryDetailViewController: MKMapViewDelegate {
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func setArtwork(location: CLLocation) {
        let artwork = Artwork(title: deliveryModel?.description ?? "",
                              locationName: deliveryModel?.description ?? "",
                              discipline: deliveryModel?.description ?? "",
                              coordinate: location.coordinate)
        mapView.addAnnotation(artwork)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard #available(iOS 11.0, *) else { return nil }
        guard let annotation = annotation as? Artwork else { return nil }
        
        let identifier = "marker"
        
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        return view
    }

}
