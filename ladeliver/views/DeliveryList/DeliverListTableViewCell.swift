//
//  DeliverListTableViewCell.swift
//  ladeliver
//
//  Created by Wu Chun Fung on 28/12/2018.
//  Copyright © 2018年 Wu Chun Fung. All rights reserved.
//

import UIKit
import SnapKit

class DeliverListTableViewCell: UITableViewCell {
    var iconImageView: UIImageView!
    var descriptionLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        iconImageView = UIImageView()
        iconImageView.backgroundColor = .lightGray
        iconImageView.contentMode = .scaleAspectFill
        iconImageView.clipsToBounds = true
        
        contentView.addSubview(iconImageView)
        iconImageView.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(100)
            make.height.equalTo(80).priority(ConstraintPriority.high)
            make.top.equalTo(contentView)
            make.left.equalTo(contentView)
            make.bottom.equalTo(contentView)
        }
        
        descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 3
        
        contentView.addSubview(descriptionLabel)
        descriptionLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(contentView)
            make.left.equalTo(iconImageView.snp.right)
            make.bottom.equalTo(contentView)
            make.right.equalTo(contentView)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
