//
//  ViewController.swift
//  ladeliver
//
//  Created by Wu Chun Fung on 27/12/2018.
//  Copyright © 2018年 Wu Chun Fung. All rights reserved.
//

import UIKit
import PromiseKit
import SnapKit
import Alamofire
import AlamofireImage

class DeliverListViewController: BaseViewController {
    var tableView: UITableView!
    var deliverList: [DeliveryItem]?
    var refreshControl = UIRefreshControl()
    
    let reuseidentifier = "DeliverListTableViewCell"
    
    override func initUI() {
        super.initUI()
        
        title = R.string.localizable.deliverList()
        
        tableView = UITableView()
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(view)
            make.left.equalTo(view)
            make.bottom.equalTo(view)
            make.right.equalTo(view)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(DeliverListTableViewCell.self, forCellReuseIdentifier: reuseidentifier)
        
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)

        getDeliveries()
    }
    
    @objc func refresh()
    {
        getDeliveries()
        refreshControl.endRefreshing()
    }
    
    func getDeliveries() {
        firstly {
            DeliverService().getdeliveries()
        }.done { response in
            self.setListData(response: response)
        }.catch { error in
            self.showAlert(msg: error.localizedDescription)
            
            if let cachedData = UIApplication.getCachedData() {
                self.setListData(response: cachedData)
            }
        }
    }
    
    func setListData(response: DeliveryResponse) {
        self.deliverList = response.deliveryItems
        
        if let count = self.deliverList?.count, count <= 0 {
            self.showAlert(msg: "Empty list\nPlease Pull to refresh")
        }
        
        self.tableView.reloadData()
    }

}

extension DeliverListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deliverList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseidentifier, for: indexPath) as? DeliverListTableViewCell
        
        guard let item: DeliveryItem = deliverList?[indexPath.row] else {
            return UITableViewCell()
        }
        
        cell?.descriptionLabel.text = item.description
        cell?.iconImageView.af_setImage(withURL: URL(string: item.imageUrl ?? "")!)
        
        cell?.selectionStyle = .none
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item: DeliveryItem = deliverList?[indexPath.row] else {
            return
        }
        
        let detailVC = DeliveryDetailViewController()
        detailVC.deliveryModel = item
        detailVC.detailModel = item.location
        pushView(vc: detailVC)
    }
}
