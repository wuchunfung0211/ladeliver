//
//  DeliveryResponse.swift
//  ladeliver
//
//  Created by Wu Chun Fung on 28/12/2018.
//  Copyright © 2018年 Wu Chun Fung. All rights reserved.
//

import Foundation
import ObjectMapper

struct DeliveryResponse: Mappable {
    
    var deliveryItems: [DeliveryItem]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        deliveryItems <- map["data"]
    }
    
}

struct DeliveryItem: Mappable {
    
    var id: Int?
    var description: String?
    var imageUrl: String?
    var location: LocationItem?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id          <- map["id"]
        description <- map["description"]
        imageUrl    <- map["imageUrl"]
        location    <- map["location"]
    }
    
}

struct LocationItem: Mappable {
    
    var lat: Double?
    var lng: Double?
    var address: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        lat     <- map["lat"]
        lng     <- map["lng"]
        address <- map["address"]
    }
    
}
